function validateForm(){
	let nombre=document.forms["FormDatos"]["nombre"].value;
	let telefono=document.forms["FormDatos"]["telefono"].value;
	let correo=document.forms["FormDatos"]["correo"].value;
	let mensaje=document.forms["FormDatos"]["mensaje"].value;

	if(nombre==""){
		alert("No lleno el campo nombre");
		return false;
	}

	if(telefono.toString().length!=12){
		alert("El telefono no debe contener mas de 12 digitos");
		return false;
	}

	if(correo==""){
		alert("No lleno el campo correo");
		return false;
	}
}